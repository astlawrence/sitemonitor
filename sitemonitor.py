#!/usr/bin/env python3
import requests
import time
import datetime
from twilio.rest import Client # sudo pip3 install twilio
 
site = "" # Site to monitor, e.g., https://blackhat.com/us-20/training/
seconds = 30
account_sid = "" # Twilio account SID
auth_token = "" # Twilio auth token
twilio_calls_url = "https://api.twilio.com/2010-04-01/Accounts/" + account_sid + "/Calls.json"
recipient = "" # Phone number in format +1XXXYYYZZZZ
magic_source = "" # Origin phone number in format +1XXXYYYZZZZ
msg_body = "" # SMS message body
callback = "" # Callback URL for status

while True:
	r = requests.get(site)
	now = datetime.datetime.now()
	print(str(now) + " - " + str(r.status_code))
	
	if r.status_code == 200:
		client = Client(account_sid, auth_token)
		
		# SMS
		#sms = client.messages.create(body=msg_body, status_callback=callback, from_=magic_source, to=recipient)
		sms = client.messages.create(body=msg_body, from_=magic_source, to=recipient)
		print("\nSMS sent at " + str(sms.date_created) + ": " + sms.sid)
		
		# Call
		call = client.calls.create(url=twilio_calls_url, to=recipient, from_=magic_source)
		print("Call made at " + str(call.start_time) + " and " + str(call.status) + " at " + str(call.end_time) + "\n")
		
		break
		
	# Sleep seconds between checks
	time.sleep(seconds)
